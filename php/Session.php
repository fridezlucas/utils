<?php

/**
 * Détruire la session et vide la superglobale $_SESSION
 * @author Fridez Lucas
 */
function deleteSession()
{
    //1. Détruire une session
    session_destroy();
    //2. Détruire le cookie de session
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time() - 1, '/');
    }
    //3. Réinitialiser la superglobale $_SESSION
    $_SESSION = [];
}