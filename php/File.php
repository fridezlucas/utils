<?php

/*****************************************************************************************
 *  PROPRIETES DES FICHIERS
 *****************************************************************************************/

/**
 * Obtenir le nom d'un fichier
 * @author Fridez Lucas
 *
 * @param $filePath : Représente le chemin du fichier dont on souhaite obtenir le nom
 *
 * @return string : Le nom du fichier désiré
 */
function getFileName($filePath) {
    return strtolower(pathinfo($filePath, PATHINFO_FILENAME));
}

/**
 * Nettoyer le nom d'un fichier en supprimant les caratères interdits
 *
 * @param $filename : Nom du fichier a nettoyer
 *
 * @return mixed|string : Nom du fichier nettoyé
 */
function sanitize_file_name($filename) {
    $filename_raw = $filename;
    $special_chars = ["?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*",
        "(", ")", "|", "~", "`", "!", "{", "}", "%", "+", chr(0)];

    $filename = preg_replace("#\x{00a0}#siu", ' ', $filename);
    $filename = str_replace($special_chars, '', $filename);
    $filename = str_replace(['%20', '+'], '-', $filename);
    $filename = preg_replace('/[\r\n\t -]+/', '-', $filename);
    $filename = trim($filename, '.-_');

    return $filename;
}

/**
 * Retourne l'extension du fichier en minuscule
 * @author Fridez Lucas
 *
 * @param $filePath : Chemin du fichier dont on veut connaître l'extension
 *
 * @return string : Extension du fichier
 */
function getExtensionFile($filePath) {
    return strtolower(pathinfo($filePath, PATHINFO_EXTENSION));
}

/**
 * Tester l'extension d'un chemin de fichier avec une extension
 * Ne contrôle pas si le fichier existe
 * @author Fridez Lucas
 *
 * @param $filePath        : Chemin du fichier dont on souhaite tester l'extension
 * @param $extensionString : Chaîne de caractère correspondant à l'extension qui sera tester sur le chemin de fichier
 *
 * @return bool : Booléen à vrai si l'extension correspond. Faux dans le cas contraire.
 */
function testExtensionFile($filePath, $extensionString) {
    return (getExtensionFile($filePath) == strtolower($extensionString));
}

/**
 * Obtenir le MIME type du fichier
 * @author Fridez Lucas
 *
 * @param $filePath : Chemin du fichir dont on veut connaître le MIME type
 *
 * @return mixed : MIME type du fichier
 */
function getMimeTypeFile($filePath) {
    $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
    if (is_file($filePath)) {
        $mimeType = finfo_file($fileInfo, $filePath);
    }
    return $mimeType;
}

/*****************************************************************************************
 *  OBTENIR LES LISTES DE FICHIERS / DOSSIERS
 *****************************************************************************************/

/**
 * Obtenir un tableau contenant tous les fichiers contenus dans un dossier
 * @author  Fridez Lucas
 *
 * @param string $dirPath       : Chemin du dossier dont on veut obtenir les fichiers
 * @param string $fileExtension : Extension de fichier souhaitée
 *
 * @return array : Tableau contenant tous les fichiers correspondants aux critères
 */
function getFiles($dirPath = __DIR__, $fileExtension = "*") {
    if (is_dir($dirPath)) {
        $files = glob($dirPath . "/*." . $fileExtension);
    }
    return $files;
}

/**
 * Obtenir un tableau contenant tous les dossiers fils contenus dans un dossier parent
 * @author  Fridez Lucas
 *
 * @param string $dirPath : Chemin du dossier dont on veut obtenir les répertoires
 *
 * @return array : Tableau contenant tous les répertoires correspondants au critère
 */
function getDirs($dirPath = __DIR__) {
    if (is_dir($dirPath)) {
        $dirs = glob($dirPath . "/*", GLOB_ONLYDIR);
    }
    return $dirs;
}

/*****************************************************************************************
 *  COMPTER LES FICHIERS / DOSSIERS
 *****************************************************************************************/

/**
 * Compter le nombre de fichiers dans un répertoire
 * @author  Fridez Lucas
 *
 * @param string $dirPath       : Chemin du dossier dont on souhaite compter le nombre de fichiers
 * @param string $fileExtension : Extension de fichier souhaitée
 *
 * @return int : Nombre de fichiers
 */
function countFiles($dirPath = __DIR__, $fileExtension = "*") {
    $files = getFiles($dirPath, $fileExtension);
    return count($files);
}

/**
 * Compter le nombre de dossiers dans un répertoire
 * @author Fridez Lucas
 *
 * @param string $dirPath : Chemin dont on désire connaître le nombre de dossiers
 *
 * @return int : Nombre de dossiers
 */
function countDirs($dirPath = __DIR__) {
    $dirs = getDirs($dirPath);
    return count($dirs);
}

/*****************************************************************************************
 *  FICHIERS XML
 *****************************************************************************************/

/**
 * Obtenir un tableau correspondant à un fichier XML
 * @author Fridez Lucas
 *
 * @param $filePath : Chemin du fichier XML à analyser
 *
 * @return array : Tableau contenant le fichier XML
 */
function getArrayXmlFile($filePath) {
    if (is_file($filePath) AND testExtensionFile($filePath, 'xml')) {
        //Parser le fichier xml
        $xml = simplexml_load_file($filePath);
        //Encoder l'objet xml en chaîne JSON
        $json = json_encode($xml);
        //Transforme la chaîne JSON en tableau PHP
        $xmlArray = json_decode($json, TRUE);
    }
    return $xmlArray;
}

/**
 * Obtenir un objet DOM Document d'un fichier XML
 * @author Fridez Lucas
 *
 * @param $filePath : Chemin du fichier XML dont on veut créer un objet DOM Document
 *
 * @return DOMDocument : DOMDocument correspondant au fichier chargé
 */
function getDomDocumentXmlFile($filePath) {
    //Créer un nouvel objet de type DOMDocument
    $xmlDoc = new DOMDocument;
    //Active le maintient de l'indentation
    $xmlDoc->formatOutput = TRUE;
    //Désactive la suppression des espaces redondants
    $xmlDoc->preserveWhiteSpace = FALSE;

    //Charger le document XML dans l'objet DOMDocument
    if (is_file($filePath) AND testExtensionFile($filePath, 'xml')) {
        $xmlDoc->load($filePath);
    }
    return $xmlDoc;
}

/*****************************************************************************************
 *  UPLOAD DE FICHIERS
 *****************************************************************************************/

//Liste des types de fichiers autorisés
const FILE_VALID_TYPE = [
    'image/gif' => 'gif',
    'text/plain' => 'txt',
    'image/jpeg' => 'jpg',
    'application/pdf' => 'pdf',
    'image/png' => 'png',
];

/**
 * Uploader un fichier s'il correspond aux critères (extension, poids, type)
 * Déplacer le fichier dans le répertoire désiré et nettoyer son nom (compatible URL)
 *
 * @param $uploadDir     : Chemin du dossier ou l'on souhaite déplacer les fichiers uploadés
 * @param $fileInputName : Nom de la balise <input type="file"> à partir de laquelle on souhaite uploader des fichiers
 *
 * @return array : Tableau contenant les erreurs d'upload. Vide si l'upload est un succès.
 */
function uploadFile($uploadDir, $fileInputName) {
    $errors = [];

    //Si erreur lors de l'envoi (code d'erreur > 0)
    if ($_FILES[$fileInputName]['error']) {
        switch ($_FILES[$fileInputName]['error']) {
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $errors[] = "Fichier trop volumineux.";
                break;
            case UPLOAD_ERR_NO_FILE:
                $errors[] = "Choisir un fichier.";
                break;
            default:
                $errors[] = "Erreur lors de l'envoi du fichier.";
                break;
        }
    }

    //Si envoi du fichier ok
    if ($_FILES[$fileInputName]['error'] === UPLOAD_ERR_OK) {
        //Détermine le type du fichier
        $fileType = getMimeTypeFile($_FILES[$fileInputName]['tmp_name']);

        //Si extension invalide
        if (!array_key_exists($fileType, FILE_VALID_TYPE)) {
            $errors[] = 'Type de fichier (' . $fileType . ') invalide.';
        }

        //Si fichier trop volumineux > MYFILES_MAX_UPLOAD
        // MYFILES_MAX_UPLOAD est une constante que l'on crée et que l'on ajoute en valeur d'un input caché.
        if ($_FILES[$fileInputName]['size'] > MYFILES_MAX_UPLOAD) {
            $errors[] = 'Fichier trop volumineux.';
        }

        //Si PAS d'erreur, on nettoye le nom du fichier et on le copie
        if (!$errors) {

            //Nettoye, supprimer les caractères interdits, du nom et supprime l'extension
            $fileName = sanitize_file_name(getFileName($_FILES[$fileInputName]['name']));

            //Ajoute l'extension correspondant au type du fichier
            $fileName = $fileName . '.' . FILE_VALID_TYPE[$fileType];

            //Copie du fichier
            if (!@move_uploaded_file($_FILES[$fileInputName]['tmp_name'], $uploadDir . $fileName)) {
                $errors[] = 'Copie du fichier' . $fileName . ' impossible pour le moment';
            }
        }
    }
    return $errors;
}