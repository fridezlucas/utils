<?php

/**
 * Retourne la chaîne date/heure passée en paramètre au format date/heure locale demandé
 *
 * @param string $time   : chaîne date/heure en anglais
 * @param string $format : format de date/heure locale http://strftime.net
 *
 * @return string : chaîne date/heure au format local encodée en UTF8
 */
function localDate($time = 'now', $format = '%d %B %Y') {
    return utf8_encode(strftime($format, strtotime($time)));
}